
# Prueba Técnica (API Developer)

```
Servicio para capturar los datos de candidatos de posibles clientes para
Grupo Unicomer para Jamaica, Guatemala y Costa Rica.
```



## Espeficicaciones

```
- Java 11
- Spring boot 2.7.8
- H2 2.1.214
- Swagger 3.0.0
```


## Deployment

```
Clonar proyectos: 
https://gitlab.com/sams029/customers
https://gitlab.com/sams029/integration-customers

cd customers
mvn clean package
docker build -t customers .

cd integration-customers
mvn clean package
docker build -t integration-customers . 
docker-compose up -d

```
# Swagger

La URL y puerto puede variar dependiendo el entorno donde se despliegue

```
http://localhost:8080/swagger-ui/#/integration-controller
http://localhost:8081/swagger-ui/#/customer-controller
http://localhost:8082/swagger-ui/#/customer-controller
http://localhost:8083/swagger-ui/#/customer-controller
```