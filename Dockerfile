FROM adoptopenjdk/openjdk11:jre-11.0.6_10-alpine
ENV PORT 8080
COPY target/*.jar /opt/app.jar
WORKDIR /opt
ENTRYPOINT exec java $JAVA_OPTS -jar app.jar