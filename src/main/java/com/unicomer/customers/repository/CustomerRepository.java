package com.unicomer.customers.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unicomer.customers.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long>{

    List<Customer> findByCustomerIdentification(String customerIdentification);
    
}
