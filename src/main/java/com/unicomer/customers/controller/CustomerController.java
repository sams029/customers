package com.unicomer.customers.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.unicomer.customers.DTO.CustomerReq;
import com.unicomer.customers.DTO.CustomerRsp;
import com.unicomer.customers.service.CustomerService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/customers")
public class CustomerController {

	@Autowired
	CustomerService customerService;

	@GetMapping("")
	public ResponseEntity<?> getCustomers() {

		try {
			log.info("Getting customers...");
			return new ResponseEntity<>(customerService.getAllCustomers(), HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.toString());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/identification/{id}")
	public ResponseEntity<?> getCustomerByIdentification(@PathVariable String id) {

		try {
			log.info("Getting customers by Identification...");
			return new ResponseEntity<>(customerService.getByCustomerIdentification(id), HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.toString());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("")
	public ResponseEntity<CustomerRsp> createCustomer(@Valid @RequestBody CustomerReq customerReq, Errors errors) {

		try {

			log.info("Checking data...");
			if (errors.hasErrors()) {
				return new ResponseEntity<>(
						new CustomerRsp("Validate", errors.getFieldError().getDefaultMessage(), Long.parseLong("0")),
						HttpStatus.BAD_REQUEST);
			}

			log.info("Creating customer...");
			return new ResponseEntity<>(customerService.saveCustormer(customerReq), HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.toString());
			return new ResponseEntity<>(
					new CustomerRsp("Error", "Please contact technical support", Long.parseLong("0")),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<CustomerRsp> changeCustomer(@Valid @RequestBody CustomerReq customerReq, Errors errors,
			@PathVariable Long id) {

		try {
			log.info("Checking data...");
			if (errors.hasErrors()) {
				return new ResponseEntity<>(
						new CustomerRsp("Validate", errors.getFieldError().getDefaultMessage(), Long.parseLong("0")),
						HttpStatus.BAD_REQUEST);
			}

			log.info("Creating customer...");
			return new ResponseEntity<>(customerService.updateCustormer(customerReq, id), HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.toString());
			return new ResponseEntity<>(
					new CustomerRsp("Error", "Please contact technical support", Long.parseLong("0")),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
