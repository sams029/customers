package com.unicomer.customers.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import com.google.gson.annotations.Expose;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Table(name = "customers")
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
	@Expose
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCustomer;

    @NotNull(message = "Customer identification must not be null")
	@Length(min = 1, max = 20, message = "Customer identification must be between 1 to 20 characters")
	private String customerIdentification;

	@NotNull(message = "First name must not be null")
	@Length(min = 1, max = 75, message = "First name must be between 1 to 75 characters")
	private String firstName;

	@NotNull(message = "Last name must not be null")
	@Length(min = 1, max = 75, message = "Last name must be between 1 to 75 characters")
	private String lastName;

	@NotNull(message = "Birthday must not be null")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDateTime birthday;

	@NotNull(message = "Gender must not be null")
	private Character gender;

	@NotNull(message = "Cell Phone must not be null")
	@Length(min = 7, max = 15, message = "Cell Phone must be between 7 to 15 characters")
	private String cellPhone;

	@NotNull(message = "Home phone must not be null")
	@Length(min = 7, max = 15, message = "Home phone must be between 7 to 15 characters")
	private String homePhone;

	@NotNull(message = "Address home must not be null")
	@Length(min = 1, max = 300, message = "Address home must be between 1 to 300 characters")
	private String addressHome;

	@NotNull(message = "Profession must not be null")
	@Length(min = 0, max = 70, message = "Profession can not be more than 70 character")
	private String profession;

	@NotNull(message = "Incomes must not be null")
	private BigDecimal incomes;
    
}
