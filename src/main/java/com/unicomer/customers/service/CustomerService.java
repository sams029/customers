package com.unicomer.customers.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ControllerAdvice;

import com.unicomer.customers.DTO.CustomerReq;
import com.unicomer.customers.DTO.CustomerRsp;
import com.unicomer.customers.model.Customer;
import com.unicomer.customers.repository.CustomerRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
@ControllerAdvice
public class CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    public List<Customer> getAllCustomers() {

        List<Customer> customers = new ArrayList<Customer>();

        try {
            customerRepository.findAll().forEach(customers::add);
            log.info("Sending customers");
        } catch (Exception e) {
            log.error(e.toString());
        }

        return customers;

    }

    public List<Customer> getByCustomerIdentification(String customerIdentification) {

        List<Customer> customers = new ArrayList<Customer>();

        try {
            customerRepository.findByCustomerIdentification(customerIdentification).forEach(customers::add);
            log.info("Sending customers");
        } catch (Exception e) {
            log.error(e.toString());
        }

        return customers;

    }

    public CustomerRsp saveCustormer(CustomerReq customerReq) {

        Customer customer;
        CustomerRsp customerRsp;

        try {
            customer = customerRepository
                    .save(new Customer(null, customerReq.getCustomerIdentification(),
                            customerReq.getFirstName(), customerReq.getLastName(), customerReq.getBirthday(),
                            customerReq.getGender(),
                            customerReq.getCellPhone(), customerReq.getHomePhone(), customerReq.getAddressHome(),
                            customerReq.getProfession(),
                            customerReq.getIncomes()));

            customerRsp = new CustomerRsp("OK", "Succesfully completed", customer.getIdCustomer());
            log.info("Customer created");
            return customerRsp;

        } catch (ConstraintViolationException ve) {
            log.error(ve.toString());
            return new CustomerRsp("Validate", getMessageError(ve), Long.parseLong("0"));
        } catch (Exception e) {
            log.error(e.toString());
            return new CustomerRsp("Error", "Please contact technical support", Long.parseLong("0"));
        }

    }

    public CustomerRsp updateCustormer(CustomerReq customerReq, Long id) {

        Customer customer;
        CustomerRsp customerRsp;

        try {

            Optional<Customer> existingCustomer = customerRepository.findById(id);

            if (existingCustomer.isPresent()) {
                customer = existingCustomer.get();
                // customer.setCountry(customerReq.getCountry());
                customer.setCustomerIdentification(customerReq.getCustomerIdentification());
                customer.setFirstName(customerReq.getFirstName());
                customer.setLastName(customerReq.getLastName());
                customer.setBirthday(customerReq.getBirthday());
                customer.setGender(customerReq.getGender());
                customer.setCellPhone(customerReq.getCellPhone());
                customer.setHomePhone(customerReq.getHomePhone());
                customer.setAddressHome(customerReq.getAddressHome());
                customer.setProfession(customerReq.getProfession());
                customer.setIncomes(customerReq.getIncomes());

                customerRsp = new CustomerRsp("OK", "Succesfully completed",
                        customerRepository.save(customer).getIdCustomer());
                log.info("Customer updated");
                return customerRsp;
            }else{
                log.info("Customer does not exist");
                return new CustomerRsp("Validate", "Customer does not exist", Long.parseLong("0"));
            }

        } catch (ConstraintViolationException ve) {
            log.error(ve.toString());
            return new CustomerRsp("Validate", getMessageError(ve), Long.parseLong("0"));
        } catch (Exception e) {
            log.error(e.toString());
            return new CustomerRsp("Error", "Please contact technical support", Long.parseLong("0"));
        }

    }

    public String getMessageError(ConstraintViolationException ve) {

        List<String> errorMessages = ve.getConstraintViolations().stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());

        return errorMessages.toString();
    }

}
